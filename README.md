# guidon

*guidon* is a french word for handlebars.  
The goal is to apply template form file structures to quickly initialize project.  
It uses handlebars templating syntax.

The guidon workspace has a library and a binary.  
The [guidon](guidon/README.md.bak) library which offers the logic and methods and [guic](guidon-cli/README.md), 
the command line frontend to the library.

Prebuilt binaries for guic are available [there](https://gitlab.com/rs-guidon/guidon/-/releases/)

## Build

### Cargo
No difficulty here. A simple `cargo build` works out of the box after cloning the project.

### Cross compilation
One can use [cross](https://github.com/rust-embedded/cross) to cross-compile the binary.  
It installs on a simple `cargo install cross`. Then a a `cross build --target x86_64-pc-windows-gnu` does the trick
for the win target (provided that the dependencies are installed).
