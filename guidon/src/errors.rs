use ansible_vault::VaultError;
use std::error::Error;
use std::{fmt, io, path};

pub type Result<T> = std::result::Result<T, GuidonError>;

#[derive(Debug)]
pub struct GuidonError {
    pub kind: ErrorKind,
    pub message: String,
}

#[derive(Debug)]
pub enum ErrorKind {
    NotFound,
    Io,
    StripPrefix,
    NotAFolder,
    GitError,
    Error,
    Crypto,
}

impl GuidonError {
    pub fn new(kind: ErrorKind, message: &str) -> Self {
        GuidonError {
            kind,
            message: message.to_string(),
        }
    }

    pub fn from_string(message: &str) -> Self {
        GuidonError {
            kind: ErrorKind::Error,
            message: message.to_string(),
        }
    }
}

impl fmt::Display for GuidonError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.message)
    }
}

impl Error for GuidonError {
    fn description(&self) -> &str {
        &self.message
    }
}

impl From<&str> for GuidonError {
    fn from(s: &str) -> Self {
        GuidonError::from_string(s)
    }
}

impl From<io::Error> for GuidonError {
    fn from(error: io::Error) -> Self {
        GuidonError {
            kind: ErrorKind::Io,
            message: error.to_string(),
        }
    }
}

impl From<path::StripPrefixError> for GuidonError {
    fn from(error: path::StripPrefixError) -> Self {
        GuidonError {
            kind: ErrorKind::StripPrefix,
            message: error.to_string(),
        }
    }
}

impl From<toml::de::Error> for GuidonError {
    fn from(error: toml::de::Error) -> Self {
        GuidonError {
            kind: ErrorKind::StripPrefix,
            message: error.to_string(),
        }
    }
}

impl From<handlebars::TemplateRenderError> for GuidonError {
    fn from(error: handlebars::TemplateRenderError) -> Self {
        GuidonError {
            kind: ErrorKind::StripPrefix,
            message: error.to_string(),
        }
    }
}

impl From<std::string::FromUtf8Error> for GuidonError {
    fn from(error: std::string::FromUtf8Error) -> Self {
        GuidonError {
            kind: ErrorKind::Error,
            message: error.to_string(),
        }
    }
}

impl From<VaultError> for GuidonError {
    fn from(e: VaultError) -> Self {
        GuidonError {
            kind: ErrorKind::Error,
            message: e.to_string(),
        }
    }
}
