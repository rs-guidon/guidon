use guidon::{CustomTemplate, GitOptions, Guidon, TryNew};
use log::{info, LevelFilter};
use pretty_assertions::assert_eq;
use std::env;
use std::fs::{read_to_string, remove_dir_all};
use std::path::Path;
use std::sync::Once;

static INIT: Once = Once::new();

fn setup() {
    INIT.call_once(|| {
        let _ = env_logger::builder()
            .is_test(true)
            .filter_level(LevelFilter::Trace)
            .try_init();
    });
}

fn get_file_content(file_num: u16) -> String {
    let fc = "This is test :X.\nAnd test :Y too.";
    let x = 2 * file_num - 1;
    let t = fc.replace(":X", &x.to_string());
    let y = file_num * 2;
    t.replace(":Y", &y.to_string())
}

#[test]
fn run_tests() {
    test_template();
    test_git_template();
    test_git_ref_branch();
    test_git_ref_tag();
    test_render_callback();
}

fn test_template() {
    setup();
    info!("Running test_template");
    let key = "Yr5(M\\@Nf4\"@PA5XLNR.F_hf{Bg`JA4=[%/3CG`(w'gA~~GRfv8v%q{JUhW\"7%L1Kt++7Uc_2hlBR-=2^KM>a5D#aIWaofoGvmO{w!lpbD,R6'ziO6eym!{%Wm9)Q#f{K\"-2[Vw'/\\BI%HlL?rU^/b]vmH?z]]j'7,jGj[7m~Oa!oN_PbupuQ*|M{C&^E`I]`huX,7{|2(2-h),V[P[xWA0z/UG.bbBO'u<|*7E$~BcUke^m;U7Ydk4L$.SF])/U";
    env::set_var("GUIDON_KEY", key);
    let path = env::current_dir().unwrap();
    println!("The current directory is {}", path.display());
    let mut guidon = Guidon::try_new("tests").unwrap();
    let to = std::env::temp_dir().join("guidon-cli-tst");
    let _ = remove_dir_all(&to);
    guidon.apply_template(&to).unwrap();

    assert_eq!(true, to.is_dir());
    let file1_path = to.join("file1.txt");
    assert_eq!(true, file1_path.is_file());
    let file1 = read_to_string(file1_path).unwrap();
    assert_eq!(get_file_content(1), file1);
    let dirs_path = to.join("tst/tplt/data");
    assert_eq!(true, dirs_path.is_dir());
    let nada_path = dirs_path.join("nada");
    assert_eq!(true, nada_path.is_file());
    let file3_path = dirs_path.join("file3.txt");
    assert_eq!(true, file3_path.is_file());
    let file3 = read_to_string(file3_path).unwrap();
    assert_eq!("Nothing to do !", file3);
    let subdirs_path = dirs_path.join("sub");
    assert_eq!(true, subdirs_path.is_dir());
    let file4_path = subdirs_path.join("file4.txt");
    assert_eq!(true, file4_path.is_file());
    let file4 = read_to_string(file4_path).unwrap();
    assert_eq!(get_file_content(4), file4);
    let my_file = subdirs_path.join("my-beautiful-file");
    assert_eq!(true, my_file.is_file());
    let dir1_path = to.join("dir1");
    assert_eq!(true, dir1_path.is_dir());
    let file2_path = dir1_path.join("file2.txt");
    assert_eq!(true, file2_path.is_file());
    let file2 = read_to_string(file2_path).unwrap();
    assert_eq!(get_file_content(2), file2);
    let file_ts_path = to.join("test_crypt");
    let file_ts = read_to_string(file_ts_path).unwrap();
    assert_eq!(
        "Top secret: Hello World!\nAnd {{ No rendering }} ?",
        file_ts
    );
}

fn test_git_template() {
    setup();
    info!("Running test_git_template");
    let from = std::env::temp_dir().join("guidon");
    let _ = remove_dir_all(&from);
    let git = GitOptions::builder()
        .repo("https://gitlab.com/rs-guidon/guidon-test-repo.git")
        .build()
        .unwrap();
    let mut guidon = Guidon::try_new(git).unwrap();
    let to = std::env::temp_dir().join("guidon-tst");
    let _ = remove_dir_all(&to);
    guidon.apply_template(&to).unwrap();

    assert_eq!(true, to.is_dir());
    let file1_path = to.join("file1.txt");
    assert_eq!(true, file1_path.is_file());
    let file1 = read_to_string(file1_path).unwrap();
    assert_eq!(get_file_content(1), file1);
    let dirs_path = to.join("tst/tplt/data");
    assert_eq!(true, dirs_path.is_dir());
    let nada_path = dirs_path.join("nada");
    assert_eq!(true, nada_path.is_file());
    let file3_path = dirs_path.join("file3.txt");
    assert_eq!(true, file3_path.is_file());
    let file3 = read_to_string(file3_path).unwrap();
    assert_eq!("Nothing to do !", file3);
    let subdirs_path = dirs_path.join("sub");
    assert_eq!(true, subdirs_path.is_dir());
    let file4_path = subdirs_path.join("file4.txt");
    assert_eq!(true, file4_path.is_file());
    let file4 = read_to_string(file4_path).unwrap();
    assert_eq!(get_file_content(4), file4);
    let my_file = subdirs_path.join("my-beautiful-file");
    assert_eq!(true, my_file.is_file());
    let dir1_path = to.join("dir1");
    assert_eq!(true, dir1_path.is_dir());
    let file2_path = dir1_path.join("file2.txt");
    assert_eq!(true, file2_path.is_file());
    let file2 = read_to_string(file2_path).unwrap();
    assert_eq!(get_file_content(2), file2);
}

fn test_git_ref_branch() {
    setup();
    info!("Running test_git_ref_branch");
    let from = std::env::temp_dir().join("guidon");
    remove_dir_all(&from).unwrap();
    let git = GitOptions::builder()
        .repo("https://gitlab.com/rs-guidon/guidon-test-repo.git")
        .rev("origin/plop")
        .build()
        .unwrap();
    let mut guidon = Guidon::try_new(git).unwrap();
    let to = std::env::temp_dir().join("guidon-tst");
    remove_dir_all(&to).unwrap();
    guidon.apply_template(&to).unwrap();

    assert_eq!(true, to.is_dir());
    let file1_path = to.join("plop_branch");
    assert_eq!(true, file1_path.is_file());
}

fn test_git_ref_tag() {
    setup();
    info!("Running test_git_ref_tag");
    let from = std::env::temp_dir().join("guidon");
    remove_dir_all(&from).unwrap();
    let git = GitOptions::builder()
        .repo("https://gitlab.com/rs-guidon/guidon-test-repo.git")
        .rev("v1")
        .build()
        .unwrap();
    let mut guidon = Guidon::try_new(git).unwrap();
    let to = std::env::temp_dir().join("guidon-tst");
    remove_dir_all(&to).unwrap();
    guidon.apply_template(&to).unwrap();

    assert_eq!(true, to.is_dir());
    let file1_path = to.join("plop_branch");
    assert_eq!(true, file1_path.is_file());
}

fn test_render_callback() {
    setup();
    info!("Running render_callback");
    let ct = CustomTemplate {
        dir_path: Path::new("tests/template"),
        template_path: Path::new("tests/template-missing.toml"),
    };
    let mut guidon = Guidon::try_new(ct).unwrap();
    guidon.set_render_callback(|h| {
        let mut s = h.clone();
        s.push_str("-cb");
        s
    });
    let to = std::env::temp_dir().join("guidon-tst");
    remove_dir_all(&to).unwrap();
    guidon.apply_template(&to).unwrap();

    assert_eq!(true, to.is_dir());
    let file1_path = to.join("file1.txt");
    assert_eq!(true, file1_path.is_file());
    let file1 = read_to_string(file1_path).unwrap();
    assert_eq!("This is test1-cb.\nAnd test2-cb too.".to_string(), file1);
}

#[test]
#[should_panic]
fn test_missing_failure_strict() {
    setup();
    info!("Running failure_strict");
    let mut guidon = Guidon::try_new("tests/template-missing.toml").unwrap();
    let to = std::env::temp_dir().join("guidon-tst");
    remove_dir_all(&to).unwrap();
    guidon.apply_template(&to).unwrap();

    assert_eq!(true, to.is_dir());
    let file1_path = to.join("file1.txt");
    assert_eq!(true, file1_path.is_file());
    let file1 = read_to_string(file1_path).unwrap();
    assert_eq!("This is test1-cb.\nAnd test2-cb too.".to_string(), file1);
}
