# guidon-cli

 `guidon-cli` is the tool using the `guidon` library to render project templates.  
 It's executable is named `guic` (as in guidon-cli...).  
 
## Features
* command line tool
* uses handlebars template syntax
* render templatized filename or folder name
* the source can be a local folder or a git repository
    * for a git repository a specific revision can be used
* the target is a folder

## Usage
 ```bash
 $ guic --help
 Usage: guic [OPTIONS]

 Apply template to project structures


 Optional arguments:
  -h, --help     help
  -v, --verbose  verbosity
  -q, --quiet    quiet mode

 Available commands:
  tplt   Templatize the given data
  crypt  Encrypt / Decrypt
````
```bash
 $ guic tptl -h
 Usage: guic tplt [OPTIONS]
 
 Positional arguments:
  from_path              Path or git repo of the template
  to_path                Output path for the target

 Optional arguments:
  -h, --help             This help message
  -g, --git-source       Required if template is hosted in a git repo
  -r, --rev REV          Revision to use
  -k, --unsecure         True to ignore certificate validation
  -a, --auto-proxy       If set to true, git tries to detect proxy configuration
  -m, --mode MODE        Render mode (lax, strict or strict_ask)
  -i, --interactive      Review variables before rendering template
  -t, --custom-tplt CUSTOM-TPLT
                         Provide a template file. If not provided, taken from given from_path.
```

```bash
 $ guic -h crypt
 Usage: guic crypt [OPTIONS]

 Positional arguments:
  data           The data to encrypt/decrypt

 Optional arguments:
  -h, --help     This help message
  -x, --decrypt  set to true to decrypt given data. If false, data encryption
  -k, --key KEY  The key to use. If not provided, the key will be read from GUIDON_KEY env var

```

## Apply from local folder
```bash
$ guic tplt /path/to/template/folder /path/to/target
```
The template file is expected te be in the folder :
```
folder
|- template.toml
|- template
|    | - file1.hbs
     | - file2.hbs
```
## Apply from template file
```bash
$ guic tplt /path/to/template_file.toml /path/to/target
```
The structure is expected to be the following: 
```
folder
|- template_file.toml
|- file1.hbs
|- file2.hbs
```

## Apply with custom template
```bash
guic tplt -t tplt_file.toml path_to/template_dir /path/to/target
```
With the given structure: 
```
|
|- tplt_file.toml
|- path_to
|    |- template_dir
     |    |- file1.hbs
          |- file2.hbs
```

## Apply from git repo
### Simple use case
If a git url is used, the ssh key must be present in ssh-agent instance.
```bash
# With a https url
$ guic tplt -g https://url.to.my/git/repo /path/to/target

# With a git/ssh url
$guic tplt -g git@git.provider:git/repo /path/to/target
```

### Private repo
With self-signed certificate and access token.  
```bash
$ guic tplt -g -k  https://:access_token@url.to.my/git/repo /path/to/target
```

With user password. You should provide url encoded user and password
```bash
$guic tplt -g https://user:password@url.to.my/git/repo /path/to/target
``` 

If *ssh-agent* is running and can provide the correct key, you can call
```bash
$guic tplt -g git@git.provider:git/rep path/to/target
```

## Templates
A basic template structure is the following :
```
a_folder/                   parent folder
  |- template.toml          configuration file (key / values)
  |- template/              folder containing templates files
    | - file1.txt.hbs       a template file (hbs extension)
    | - file{{key1}}        the {{key1}} part will be replaced
    | - dir1                
    |   |- file2.txt.hbs    a template file
    |   |- file3.txt        this file will be copied to destination
    | - {{dirkey}}          this folder will be renamed
    |   |- {{filekey}}.hbs  this file will be renamed and rendered
```

And a `file1.txt.hbs` file like:
```
This is a {{key2}}, but it could have been a {{key3}}.
```

With a `template.toml` file like the following :
```toml
[variables]
key1 = ".bak"
key2 = "plop"
key3 = "toto"
dirkey = "THE/FOLDER"
filekey = "my_file.ini"
```
The output structure will be:
```
target_folder/
    |- file1.txt
    |- file.bak
    |- dir1/
    |   |- file2.txt
    |   |- file3.txt
    |- THE/
        |- FOLDER/
            |- my_file.ini
```
and the `file1.txt` content will be:
```
This is a plop, but it could have been a toto.
```

> Some helpers are provided, see [the readme](guidon/README.md#helpers)  

### Render modes
If `lax`, missing values are defaulted to an empty string,
if `strict` an error is raised for a missing value,
if `strict_ask`, user is prompted for missing values.

## Installation

### via Cargo
```bash
$ cargo install --force guidon-cli
```

### Prebuilt binaries
Prebuilt binaries are available [there](https://gitlab.com/rs-guidon/guidon/-/releases/)
 
 