// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

mod cliopts;

use std::collections::BTreeMap;
use std::io::Write;
use std::path::Path;

use crate::cliopts::{CliOpts, Command, Mode, TemplateOpts};
use guidon::{gdecrypt, gencrypt, CustomTemplate, GitOptions, Guidon, GuidonError, Result, TryNew};
use log::{error, info};

const VERSION: &str = env!("CARGO_PKG_VERSION");

fn main() {
    if let Err(e) = guic_main() {
        error!("Guic error : {}", e.message);
        std::process::exit(1);
    }
    info!("Done.");
}

fn guic_main() -> Result<()> {
    let opts: CliOpts = CliOpts::parse_args_or_error().unwrap_or_else(|e| {
        eprintln!("Invalid args: {}", e);
        let opts: CliOpts = Default::default();
        opts.show_usage();
        std::process::exit(2);
    });

    init_logs(opts.verbose, opts.quiet);

    if let Some(cmd) = opts.command {
        match cmd {
            Command::Template(tplt) => guic_template(&tplt)?,
            Command::Crypt(crypt) => {
                if crypt.decrypt {
                    let msg = gdecrypt(&crypt.data, crypt.key.as_deref())?;
                    println!("Decrypted data : {}", msg);
                } else {
                    let msg = gencrypt(&crypt.data, crypt.key.as_deref())?;
                    println!("Encrypted data : {}", msg);
                }
            }
        }
    } else {
        opts.show_usage();
    }
    Ok(())
}

fn init_logs(verbosity: usize, quiet: bool) {
    let verbosity = if verbosity == 0 { 3 } else { verbosity };
    stderrlog::new()
        .module(module_path!())
        .quiet(quiet)
        .verbosity(verbosity)
        .init()
        .unwrap();
    info!("GUIC v{}", VERSION);
}

fn init_guidon(opts: &TemplateOpts) -> Result<Guidon> {
    if opts.git_source {
        let git_opts = opts
            .git_opts()
            .ok_or_else(|| GuidonError::from_string("Invalid git options"))?;
        let mut builder = GitOptions::builder();
        builder.repo(opts.from_path.clone());
        builder.unsecure(git_opts.unsecure);
        builder.auto_proxy(git_opts.auto_proxy);
        if let Some(r) = git_opts.rev {
            builder.rev(r);
        }
        let git_options = builder.build().map_err(|e| GuidonError::from_string(&e))?;

        Guidon::try_new(git_options)
    } else if opts.custom_tplt.is_none() {
        Guidon::try_new(&opts.from_path)
    } else {
        let template_path = Path::new(opts.custom_tplt.as_ref().unwrap());
        Guidon::try_new(CustomTemplate {
            dir_path: Path::new(&opts.from_path),
            template_path,
        })
    }
}

fn guic_template(opts: &TemplateOpts) -> Result<()> {
    let mut guidon = init_guidon(opts)?;
    match opts.mode {
        Mode::Strict => {
            guidon.use_strict_mode(true);
        }
        Mode::Lax => {
            guidon.use_strict_mode(false);
        }
        Mode::StrictAsk => {
            guidon.use_strict_mode(true);
            guidon.set_render_callback(render_callback);
        }
    }
    if opts.interactive {
        guidon.set_variables_callback(variables_callback)
    }
    guidon.apply_template(&opts.to_path)
}

fn render_callback(key: String) -> String {
    let mut data = String::new();
    print!(" - {} ? []: ", key);
    let _ = std::io::stdout().flush();
    let _ = std::io::stdin().read_line(&mut data);
    let mut r = data.split(':').next().unwrap();
    r = r.trim();
    if r.is_empty() {
        r = "";
    }
    r.to_string()
}

fn variables_callback(vars: &mut BTreeMap<String, String>) {
    for (key, value) in vars.iter_mut() {
        let mut data = String::new();
        print!(" - {} ? [{}]: ", key, value);
        let _ = std::io::stdout().flush();
        let _ = std::io::stdin().read_line(&mut data);
        let mut r = data.split(':').next().unwrap();
        r = r.trim();
        if !r.is_empty() {
            *value = r.to_string();
        }
    }
}
