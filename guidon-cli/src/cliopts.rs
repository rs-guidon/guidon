use guidon::{GuidonError, Result};
use gumdrop::{Options, ParsingStyle};
use std::env::args;
use std::str::FromStr;

/// Apply template to project structures
#[derive(Options, Debug, Default)]
pub struct CliOpts {
    #[options(help = "help")]
    pub help: bool,
    /// Verbose mode (-v: logs, -vv: more logs, -vvv: logs... and logs...)
    #[options(help = "verbosity", count)]
    pub verbose: usize,
    /// Quiet mode : no logs
    #[options(help = "Quiet mode")]
    pub quiet: bool,
    #[options(command)]
    pub command: Option<Command>,
}

impl CliOpts {
    /// Parses arguments from the environment.
    ///
    /// If the user supplies a help option, option usage will be printed to
    /// `stderr` and the process will exit with status code `0`.
    ///
    /// Otherwise, the parsed options are returned.
    pub fn parse_args_or_error() -> std::result::Result<Self, gumdrop::Error>
    where
        Self: Sized,
    {
        use std::process::exit;

        let args = args().collect::<Vec<_>>();

        let opts = Self::parse_args(&args[1..], ParsingStyle::AllOptions)?;

        if opts.help_requested() {
            opts.show_usage();
            exit(0);
        }

        Ok(opts)
    }

    pub fn show_usage(&self) {
        let args = args().collect::<Vec<_>>();
        let mut command = self as &dyn Options;
        let mut command_str = String::new();

        while let Some(new_command) = command.command() {
            command = new_command;
            if let Some(name) = new_command.command_name() {
                command_str.push(' ');
                command_str.push_str(name);
            }
        }

        eprintln!("Usage: {}{} [OPTIONS]", args[0], command_str);
        eprintln!();
        eprintln!("{}", command.self_usage());

        if let Some(cmds) = command.self_command_list() {
            eprintln!();
            eprintln!("Available commands:");
            eprintln!("{}", cmds);
        }
    }
}

#[derive(Debug, Options)]
pub enum Command {
    #[options(help = "Templatize the given data", name = "tplt")]
    Template(TemplateOpts),
    #[options(help = "Encrypt / Decrypt")]
    Crypt(CryptoOpts),
}

#[derive(Debug, Options)]
pub struct CryptoOpts {
    #[options(help = "Help message")]
    pub help: bool,
    #[options(
        help = "set to true to decrypt given data. If false, data encryption",
        short = "x"
    )]
    pub decrypt: bool,
    #[options(
        help = "The key to use. If not provided, the key will be read from GUIDON_KEY env var",
        short = "k"
    )]
    pub key: Option<String>,
    #[options(help = "The data to encrypt/decrypt", free)]
    pub data: String,
}

#[derive(Debug, Options)]
pub struct TemplateOpts {
    #[options(help = "Help message")]
    pub help: bool,
    /// Git source : use this flag if the template is hosted in a git repo
    #[options(help = "Required if template is hosted in a git repo")]
    pub git_source: bool,
    /// git - Revision of the repository to use.
    ///
    /// Can be a tag (`1.0`), a branch (`origin/my_branch`) or a revision id.
    /// By default the value is set to `master`
    #[options(help = "Revision to use")]
    pub rev: Option<String>,
    /// git - If set to `true`, certificate validity is not checked
    #[options(help = "True to ignore certificate validation", short = "k")]
    pub unsecure: bool,
    /// git - If set to `true`, git tries to detect proxy configuration
    #[options(help = "If set to true, git tries to detect proxy configuration")]
    pub auto_proxy: bool,
    /// Path of the template directory
    ///
    /// If the git flag is specified, it's the URL of the git repo.
    /// Else it's the folder where the template can be found.
    /// If the path resolves to a file, guic assumes it's the config file,
    /// and that the its parent dir is the working dir.
    #[options(help = "Path or git repo of the template", free)]
    pub from_path: String,
    /// Output path for the target
    #[options(help = "Output path for the target", free)]
    pub to_path: String,
    /// Render mode.
    ///
    /// If `lax`, missing values are defaulted to an empty string,
    /// if `strict` an error is raised for a missing value,
    /// if `strict_ask`, user is prompted for missing values.
    #[options(help = "Render mode (lax, strict or strict_ask)", parse(try_from_str))]
    pub mode: Mode,
    /// Interacive : ask for variables values at runtime
    #[options(
        help = "Review variables before rendering template",
        long = "interactive",
        short = "i"
    )]
    pub interactive: bool,
    /// Wether the template files are located in a template folder or not.
    ///
    /// By default a template dir is used. Its name is always `template`.
    #[options(help = "If a custom template is used.", short = "t")]
    pub custom_tplt: Option<String>,
}

impl TemplateOpts {
    pub(crate) fn git_opts(&self) -> Option<GitOpts> {
        if self.git_source {
            Some(GitOpts {
                rev: self.rev.as_ref(),
                unsecure: self.unsecure,
                auto_proxy: self.auto_proxy,
            })
        } else {
            None
        }
    }
}

#[derive(Debug)]
pub enum Mode {
    Lax,
    Strict,
    StrictAsk,
}

impl Default for Mode {
    fn default() -> Self {
        Mode::Strict
    }
}

impl FromStr for Mode {
    type Err = GuidonError;

    fn from_str(mode: &str) -> Result<Self> {
        match mode {
            "lax" => Ok(Mode::Lax),
            "strict" => Ok(Mode::Strict),
            "strict_ask" => Ok(Mode::StrictAsk),
            _ => Err(GuidonError::from_string(&format!(
                "Could not parse {}",
                mode
            ))),
        }
    }
}

#[derive(Debug)]
pub struct GitOpts<'a> {
    /// git - Revision of the repository to use.
    ///
    /// Can be a tag (`1.0`), a branch (`origin/my_branch`) or a revision id.
    /// By default the value is set to `master`
    pub rev: Option<&'a String>,
    /// git - If set to `true`, certificate validity is not checked
    pub unsecure: bool,
    /// git - If set to `true`, git tries to detect proxy configuration
    pub auto_proxy: bool,
}
